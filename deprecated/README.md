This dir is to temperarily store scripts and instructions that I am playing with


###Or alternatively, cluster by connected components
	
	python all-by-all_blastp_to_edges.py <raw blastp output>
	python dna_find_connected_components.py <.edges file> <outDIR>
	python write_fasta_files_from_ccs.py <fasta file of all sequences> <.ccs file> <outDIR> <seq_len_filter>


###Separate very large clusters formed by multi-domain protein families

	python pep_find_subclusters_for_large_families.py DIR file_ending


##Clustering transcripts

Directly using transcripts only works when the group is very closely-related.

	cat *.fasta >all_mrna.fa
	makeblastdb -in all_mrna.fa -parse_seqids -dbtype nucl -out all_mrna.fa
	blastn -db all_mrna.fa -query all_mrna.fa -evalue 0.00001 -num_threads 9 -max_target_seqs 200 -out all_mrna.rawblastn -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'

For transcripts, only use the top hit for filtering blast hits. Do not use hit fraction. Hit fraction often includes chimeras.

	python all-by-all_mrna_blastn_to_mcl.py all_mrna.rawblastn 0.3

Use a more stringent set of MCL parameters to produce tight clusters. 

	mcl all_mrna.rawblastn.coverage0.3.log-evalue --abc -te 10 -tf 'gq(20)' -I 3 -o coverage_I3_e20

Flip direction. Clusters have to be tight in order for flipping direction to work.

	python ~/phylogenomic_dataset_construction/flip_mrna_direction_and_cut_ends.py <DIR with fasta file for each cluster> <num_cores>

